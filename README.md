# Qr-Demo

Demo de creacion de QR con ZXing.Net

### Ejemplo de códificacion para generar QR

```c#
 BarcodeWriter br = new BarcodeWriter();
 br.Format = format;
            
 br.Options = new ZXing.Common.EncodingOptions { Height = 400, Width = 400 };

 Bitmap bm = new Bitmap(br.Write("QR con texto simple"));
 
 bm.Save("c:\\qr-example\\qr-example.png", ImageFormat.Png);
```


### Ejecución del proyecto
![alt text](images/console-app-demo.png "Consola app")


### Resultado
![alt text](images/qr-generados.png "Resultado")