﻿using System;
using System.IO;
using ZXing;
using System.Drawing;
using System.Drawing.Imaging;

namespace qr_code_example
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "C:\\qr-example\\";
            Console.WriteLine("Iniciando Demo");
            CreateDirectory(path);

            /// QR SIMPLE
            Console.WriteLine("Creando QRs");
            var qr01 = GenerateQr_zxing("QR con un texto simple", BarcodeFormat.QR_CODE);
            saveQr(qr01, path, string.Format("qr-simple-{0}.png", DateTime.Now.ToString("ddmmyyyyhhmmss")), ImageFormat.Png);

            /// QR con imagen y texto
            
            var qr02 = GenerateQr_zxing("QR con un texto simple y texto externo e imagen", BarcodeFormat.QR_CODE);
            addText_Image(qr02, "Texto Externo");
            saveQr(qr02, path, string.Format("qr-with-text-image-{0}.png", DateTime.Now.ToString("ddmmyyyyhhmmss")), ImageFormat.Png);


            Close();
        }

        static void Close()
        {
            Console.WriteLine("Demo finalizada");
            Console.WriteLine("Presione cualquier tecla para salir..");
            Console.ReadKey();
        }

        static void addText_Image(Bitmap bitmap, string text)
        {
            // Agrega texto
                    Graphics graphicsImage = Graphics.FromImage(bitmap);
                    StringFormat stringformat = new StringFormat();
                    stringformat.Alignment = StringAlignment.Center;
                    stringformat.LineAlignment = StringAlignment.Far;
                    Color StringColor = System.Drawing.ColorTranslator.FromHtml("#151515");
                    graphicsImage.DrawString(text, new Font("arial", 10, FontStyle.Regular), new SolidBrush(StringColor), new Point((int)(81.8582 + (-19535760 - 81.8582) / (1 + Math.Pow(text.Length / 0.01051845, 2.306843))), 37), stringformat);


            //Agrega Imagen            
            string pathLogo = Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.FullName, "image\\logo-everis.png");
            Bitmap logo = new Bitmap(pathLogo);
            graphicsImage.DrawImage(logo, new Rectangle(bitmap.Width - logo.Width - 25, 0, logo.Width - 23, logo.Height - 10));
        }

        static void CreateDirectory(string path)
        {
            Console.WriteLine(string.Format("Validando existencia del directorio \"{0}\"", path));
            if (!Directory.Exists(path))
            {
                Console.WriteLine(string.Format("Creando directorio \"{0}\"", path));
                Directory.CreateDirectory(path);
            }
            else
            {
                Console.WriteLine(string.Format("El directorio existe", path));
            }
        }

        static void saveQr(Bitmap bitmap, string path, string name, ImageFormat imageFormat)
        {
            bitmap.Save(string.Format("{0}{1}", path, name), imageFormat);
        }

        #region QR ZXING

        static Bitmap GenerateQr_zxing(string texto, BarcodeFormat format)
        {
            BarcodeWriter br = new BarcodeWriter();
            br.Format = format;
            
            br.Options = new ZXing.Common.EncodingOptions { Height = 400, Width = 400 };

            Bitmap bm = new Bitmap(br.Write(texto));
            return bm;
        }
        #endregion
    }
}
